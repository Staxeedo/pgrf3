package cz.uhk.pgrf3.madr.cv6;

import lvl2advanced.p01gui.p01simple.LwjglWindow;

import java.io.IOException;

public class App {

	public static void main(String[] args) throws IOException {
		new LwjglWindow(new Renderer());
	}

}