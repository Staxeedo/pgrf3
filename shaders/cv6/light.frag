#version 150
in vec4 positi;
out vec4 outColor; // output from the fragment shader

void main() {
    outColor = vec4(vec3(positi.z/positi.w),1.0);
} 
