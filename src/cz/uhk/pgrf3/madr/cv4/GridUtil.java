package cz.uhk.pgrf3.madr.cv4;

public class GridUtil {
   public  static float[] createVB(int m, int n)
   {
       if(m<1||n<1)
       {
           return null;
       }

       float[] vertexbuff= new float[(m+1)*(n+1)*2];
       int index = 0;
       for(int i = 0; i<=n;i++) {
           for (float j = 0; j <= m; j++) {
                  vertexbuff[index++]=j*1f/(m);
                  vertexbuff[index++]=i*1f/(n);
               }

       }
       for(float i : vertexbuff) {
           System.out.print(i + " ");
       }
       return vertexbuff;
   }

    public static int[] createIB(int m, int n)
    {
        int[] indexbuff= new int[m*n*6];


        int index = 0;
        int k = 0;
        for(int y = 0; y<n;y++) {

            for(int x= 0;x<m;x++)
            {
                k=x+y*(m+1);
                indexbuff[index++]=k;
                indexbuff[index++]=k+(m+1);
                indexbuff[index++]=k+(m+1)+1;
                indexbuff[index++]=k;
                indexbuff[index++]=k+(m+1)+1;
                indexbuff[index++]=k+1;
            }

        }

        for(int i : indexbuff) {
            System.out.print(i + " ");
        }

        return indexbuff;
    }
}
