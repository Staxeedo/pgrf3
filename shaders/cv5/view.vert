#version 150
in vec2 inPosition; // input from the vertex buffer
out vec3 color;
out vec3 normal_IO;
out vec3 lightDir;
out vec2 posIO;
out float intensity;
out vec3 viewDirection;
out float distance;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform float type;
uniform float time;
uniform int perVertex;
uniform int anim;
uniform float lightPos;
uniform float func;
uniform float pi;

vec3 getCartesianTorus(vec2 xy){
    float t = xy.x* pi* 2.01;
    float s = xy.y * pi * 2.01;

    float x = (3*cos(s)+cos(t)*cos(s))/3;
    float y = (3*sin(s)+cos(t)*sin(s))/3;
    float z = sin(t)/3;
    return vec3(x,y,z);
}

vec3 getCartesianOwn(vec2 xy){
    float t = xy.x* 3.14* 2.01;
    float s = xy.y * 3.14 * 2.01;

    float x = cos(s)+cos(t)*cos(s)/1.5;
    float y = sin(s)+cos(t)*sin(s)/1.5;
    float z = sin(t)*cos(t)/2;
    return vec3(x,y,z);
}

vec3 getSphereNut(vec2 xy){
    float t = xy.x * 3.14;
    float s = xy.y * 3.14 * 1.5;

    float phi = t;
    float rho = (sin(t)*cos(3.14/2*sin(t)*cos(s))/2);
    float theta = s;

    float x = 3*rho * sin(phi) * cos(theta);
    float y = 3*rho * sin(phi) * sin(theta);
    float z = 3*rho * cos(phi);
    return vec3(x,y,z);
}


vec3 getSphereOwn(vec2 xy){
    float t = xy.x* 3.14* 0.5;
    float s = xy.y * 3.14;

    float phi = t*t/2;
    float rho = abs(t-pi/10);
    float theta = s*4;

    float x = rho * sin(phi) * cos(theta);
    float y = rho * sin(phi) * sin(theta);
    float z = rho * cos(phi);
    return vec3(x,y,z);
}

vec3 getSphereNutNormal(vec2 xy){

    vec3 dx = getSphereNut(xy + vec2(0.001,0)) - getSphereNut(xy - vec2(0.001,0));

    vec3 dy = getSphereNut(xy + vec2(0,0.001)) - getSphereNut(xy - vec2(0,0.001));
    return cross(dx,dy);
}
vec3 getSphereOwnNormal(vec2 xy){

    vec3 dx = getSphereOwn(xy + vec2(0.001,0)) - getSphereOwn(xy - vec2(0.001,0));

    vec3 dy = getSphereOwn(xy + vec2(0,0.001)) - getSphereOwn(xy - vec2(0,0.001));
    return cross(dx,dy);
}
vec3 getCylindricGoblet(vec2 xy){
    float t = xy.x* 3.14* 2;
    float s = xy.y * 3.14 *1.01 - (-xy.y * 3.14);

    float r = 1+sin(t);
    float z = -t/2+1;
    float theta = s;

    float x = (r * cos(theta))/3;
    float y = (r * sin(theta))/3;
    return vec3(x,y,z);
}
vec3 getCylindricOwn(vec2 xy){
    float t = xy.x* 3.14* 2.01;
    float s = xy.y * 3.14*1.01 - (-xy.y * 3.14);

    float r = 1+cos(t)*cos(t)*sin(t)*sin(t)*10;
    float z = t/3 - 1.5;
    float theta = s;

    float x = (r * cos(theta))/5;
    float y = (r * sin(theta))/5;
    return vec3(x,y,z);
}
vec3 getCylindricGobletNormal(vec2 xy){

    vec3 dx = getCylindricGoblet(xy + vec2(0.001,0)) - getCylindricGoblet(xy - vec2(0.001,0));

    vec3 dy = getCylindricGoblet(xy + vec2(0,0.001)) - getCylindricGoblet(xy - vec2(0,0.001));
    return cross(dx,dy);
}
vec3 getCylindricOwnNormal(vec2 xy){

    vec3 dx = getCylindricOwn(xy + vec2(0.001,0)) - getCylindricOwn(xy - vec2(0.001,0));

    vec3 dy = getCylindricOwn(xy + vec2(0,0.001)) - getCylindricOwn(xy - vec2(0,0.001));
    return cross(dx,dy);
}

float getFValue(vec2 position){
    float scale = 2;
    float distance = position.x*position.x*scale + position.y*position.y*scale;
    return -(distance);
}

vec3 getNormal(vec2 xy){
    //osvetleni
    float delta = 0.01;
    vec3 u = vec3(xy.x + delta, xy.y, getFValue(xy + vec2(delta,0)))
    - vec3(xy -  vec2(delta,0), getFValue(xy - vec2(delta,0)));
    vec3 v = vec3(xy + vec2(0,delta), getFValue(xy + vec2(0,delta)))
    - vec3(xy - vec2(0,delta), getFValue(xy - vec2(0,delta)));
    return cross(u,v);
}

void main() {
    vec3 light = vec3(10);
	vec2 position = inPosition;

	posIO = inPosition.yx;

    vec4 objPos;
    vec3 normal;
    vec3 pos = vec3(position.x,position.y,1.0);
    pos.xy -= 0.5;
    vec3 finalpos;
    //pokrocile funkce
    if(func == 1){
        finalpos = getCartesianTorus(pos.xy);
        normal = getNormal(finalpos.xy);
    }else if(func == 2){
        finalpos = getCartesianOwn(pos.xy);
        normal = getNormal(finalpos.xy);
    }else if(func == 3){
        finalpos = getSphereNut(pos.xy);
        normal = getSphereNutNormal(pos.xy);
    } else if(func == 4){
        finalpos = getSphereOwn(position.xy);
        normal = getSphereOwnNormal(pos.xy);
    } else if(func == 5){
        finalpos = getCylindricGoblet(pos.xy);
        normal = getCylindricGobletNormal(pos.xy);
    }  else if(func == 6){
        finalpos = getCylindricOwn(pos.xy);
        normal = getCylindricOwnNormal(pos.xy);
    }
    float z = getFValue(pos.xy);

    if(func == 0){
        //zakladni funkce
        objPos = vec4(pos.x, z,pos.y, 1.0);
        normal = normalize(getNormal(pos.xy));
    } else {
        objPos = vec4(finalpos,1.0);
    }

    light = vec3(light.x - objPos.x , light.y - objPos.y, light.z - objPos.z);

    normal = inverse(transpose(mat3(view*model)))*normal;
    lightDir = normalize(light - (view*model*objPos).xyz);
    normal_IO = normal;
    color = vec3(normal);

    float offset = time * 2.5;
    if(anim == 0){
        objPos.z += 0.5 * sin(1.5* objPos.z + offset);
    } else if(anim == 1){
        objPos.y += 0.2 * sin(2.5* objPos.x + offset);
    }
    gl_Position = proj*view*model*objPos;
    intensity = dot(normalize(lightDir), normalize(normal));
    viewDirection = - (objPos.xyz);
    distance = length(lightDir);

}
